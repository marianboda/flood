import { Promise } from 'bluebird'

export const hexColors = [
  '#FF4444',
  '#FF9174',
  '#FFDF4C',
  '#E3F100',
  '#7DF100',
  '#00F1B5',
  '#00DDF1',
  '#64a3f8',
  '#A195FF',
  '#D592FF',
  '#FF92F3',
  '#FD638B',
  '#FFFFFF',
  '#CCCCCC',
  '#999999',
  '#666666',
]

export const colors = hexColors.map(i => parseInt(`${i.substr(1)}FF`, 16))

export function getRandomColor() {
  return colors[Math.floor(Math.random() * colors.length)]
}

export function loadImage(url) {
  const image = new Image()
  const p = new Promise((resolve, reject) => {
    image.onload = ({ target }) => {
      if (image.width > 0 && image.height > 0) {
        return resolve(target)
      }
      return reject('Error: image loading failed')
    }
  })

  image.src = url
  return p
}

export function intToBytes(num) {
  const buffer = new ArrayBuffer(4)
  const view = new DataView(buffer)
  view.setUint32(0, num, false)
  return new Uint8Array(buffer)
}

export function getPixelColor(image, x, y) {
  const offset = 4 * ((image.width * y) + x)
  const color = image.data.slice(offset, offset + 4)
  return color
}

export function setPixelColor(image, x, y, color) {
  const offset = 4 * ((image.width * y) + x)
  image.data[offset] = color[0]
  image.data[offset + 1] = color[1]
  image.data[offset + 2] = color[2]
  image.data[offset + 3] = color[3]
}

export function colorMatch(col1, col2) {
  return (col1[0] === col2[0])
    && (col1[1] === col2[1])
    && (col1[2] === col2[2])
    && (col1[3] === col2[3])
}
