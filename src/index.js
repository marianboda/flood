import 'file-loader?name=[name].[ext]!../index.html'
import 'file-loader?name=[path]/[name].[ext]!../img/pikes.png'

import {
  loadImage,
  intToBytes,
  colors,
  hexColors,
 } from './drawing-utils'
import fill from './flood-fill'

const canvas = document.getElementById('canvas')
const ctx = canvas.getContext('2d')
let image = ctx.getImageData(0, 0, canvas.width, canvas.height)
let fillColor = 0

function selectColor(i) {
  const oldSelectedColor = document.getElementById(`color-${fillColor}`)
  oldSelectedColor.classList.remove('active')
  fillColor = i
  const newSelectedColor = document.getElementById(`color-${i}`)
  newSelectedColor.classList.add('active')
}

function generateColorButtons() {
  hexColors.forEach((c, idx) => {
    const btn = document.createElement('button')
    btn.setAttribute('id', `color-${idx}`)
    btn.style.backgroundColor = c
    btn.className = 'color-btn'
    btn.addEventListener('click', () => selectColor(idx))
    document.getElementById('colors').appendChild(btn)
  })
}

function redraw(screen) {
  ctx.putImageData(screen, 0, 0)
}

canvas.addEventListener('click', ({ layerX: x, layerY: y }) => {
  const color = intToBytes(colors[fillColor])
  fill(image, x, y, color)
  redraw(image)
})

generateColorButtons()
selectColor(0)

loadImage('img/pikes.png').then((resultImage) => {
  ctx.drawImage(resultImage, 0, 0)
  image = ctx.getImageData(0, 0, canvas.width, canvas.height)
})
