import {
  colorMatch,
  getPixelColor,
  setPixelColor,
} from './drawing-utils'

// floodPoint function colors a point and returns a list of neighbors
function floodPoint(screen, x, y, newColor) {
  setPixelColor(screen, x, y, newColor)

  const nextPoints = [
    { x: x + 1, y },
    { x: x - 1, y },
    { x, y: y + 1 },
    { x, y: y - 1 },
  ]
  return nextPoints
}

// testPoint checks if point is not out of bounds and if matches original color
function testPoint(screen, x, y, origColor) {
  return (
    (x < screen.width)
    && (x >= 0)
    && (y < screen.height)
    && (y >= 0)
    && (colorMatch(getPixelColor(screen, x, y), origColor))
  )
}

// processQueue processes the queue of points until it's empty
function processQueue(queue, screen, origColor, color) {
  while (queue.length > 0) {
    const point = queue.shift()
    if (testPoint(screen, point.x, point.y, origColor)) {
      const nextPoints = floodPoint(screen, point.x, point.y, color)
      queue.push(...nextPoints)
    }
  }
}

// Function to start the flood-fill
export default function fill(screen, x, y, color) {
  const origColor = getPixelColor(screen, x, y)

  // If the original color and the desired one are equal, there is no change needed.
  if (colorMatch(origColor, color)) {
    return
  }
  const queue = []
  queue.push({ x, y })

  processQueue(queue, screen, origColor, color)
}
