# Flood fill

Flood fill algorithm is implemented in [flood-fill.js](src/flood-fill.js) file.

## Algorithm
It is a basic algorithm starting with coloring point 0 and for every neighbor with the same color puts it to the queue to be processed in the same way.

Recursive version was running into problems because of the big call stack required.

Solution could be improved by using the Scanline fill algorithm.


## Demo
Live demo is located [here](http://vacuum.boda.one/flood).
